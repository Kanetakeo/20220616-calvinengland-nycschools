package com.example.nyc_schools_test.model.remote

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class SchoolList(
    @SerializedName("city")
    val city: String,
    @SerializedName("dbn")
    val dbn: String,
    @SerializedName("latitude")
    val latitude: String?,
    @SerializedName("location")
    val location: String,
    @SerializedName("longitude")
    val longitude: String?,
    @SerializedName("overview_paragraph")
    val overviewParagraph: String,
    @SerializedName("phone_number")
    val phoneNumber: String,
    @SerializedName("school_email")
    val schoolEmail: String,
    @SerializedName("school_name")
    val schoolName: String,
    @SerializedName("state_code")
    val stateCode: String,
    @SerializedName("website")
    val website: String,
    @SerializedName("zip")
    val zip: String
): Serializable

data class SatSchool(
    val dbn: String,
    @SerializedName("num_of_sat_test_takers")
    val satTestTakers: String,
    @SerializedName("sat_critical_reading_avg_score")
    val readingAvg: String,
    @SerializedName("sat_math_avg_score")
    val mathAvg: String,
    @SerializedName("sat_writing_avg_score")
    val writingAvg: String
)
