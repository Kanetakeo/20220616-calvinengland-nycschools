package com.example.nyc_schools_test.model

import com.example.nyc_schools_test.model.remote.SchoolList
import com.example.nyc_schools_test.model.remote.SatSchool

sealed class ResultState {
    data class Loading(val isLoading: Boolean = false): ResultState()
    data class SuccessSchoolList(val data: List<SchoolList>): ResultState()
    data class SuccessSchoolSat(val data: SatSchool?): ResultState()
    data class Error(val errorMessage: String): ResultState()
}
