package com.example.nyc_schools_test.model

import com.example.nyc_schools_test.common.NetworkManager
import com.example.nyc_schools_test.model.remote.NycApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

interface Repository {
    fun caseSchoolList(): Flow<ResultState>
    fun caseSchoolSatByDBN(dbn: String): Flow<ResultState>
}

class RepositoryImpl @Inject constructor(
    private val networkManager: NetworkManager,
    private val service: NycApi
) : Repository {

    override fun caseSchoolList(): Flow<ResultState> {
        return flow {
            emit(ResultState.Loading(true))
            delay(500)

            if (networkManager.isConnected) {
                val response = service.getSchoolList()
                if (response.isSuccessful) {
                    response.body()?.let { remote ->
                        emit(ResultState.SuccessSchoolList(remote))
                    }
                } else {
                    emit(ResultState.Error(response.errorBody()?.string().toString()))
                }
            } else {
                emit(ResultState.Error("No data available"))
            }
        }
    }

    override fun caseSchoolSatByDBN(dbn: String): Flow<ResultState> = flow {
        emit(ResultState.Loading(true))
        delay(500)

        if (networkManager.isConnected) {
            val response = service.getSchoolSat()
            if (response.isSuccessful) {
                response.body()?.let { remote ->
                    emit(ResultState.SuccessSchoolSat(remote.firstOrNull { it.dbn == dbn }))
                }
            } else {
                emit(ResultState.Error(response.errorBody()?.string().toString()))
            }
        } else {
            emit(ResultState.Error("No data available"))
        }
    }
}