package com.example.nyc_schools_test.di

import com.example.nyc_schools_test.model.Repository
import com.example.nyc_schools_test.model.RepositoryImpl
import dagger.Binds
import dagger.Module

@Module
abstract class RepositoryModule {

    @Binds
    abstract fun providesRepoLayer(repositoryImpl: RepositoryImpl): Repository
}