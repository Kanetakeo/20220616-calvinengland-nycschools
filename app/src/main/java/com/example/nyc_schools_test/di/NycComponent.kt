package com.example.nyc_schools_test.di

import com.example.nyc_schools_test.MainActivity
import com.example.nyc_schools_test.view.FragmentSchool
import com.example.nyc_schools_test.view.ScoreFragment
import dagger.Component

@Component(
    modules = [
        ApplicationModule::class,
        NetworkModule::class,
        RepositoryModule::class
    ]
)
interface NycComponent {
    fun inject(mainActivity: MainActivity)
    fun inject(schoolFragment: FragmentSchool)
    fun inject(scoreFragment: ScoreFragment)
}