package com.example.nyc_schools_test.view

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.nyc_schools_test.model.Repository
import com.example.nyc_schools_test.viewmodel.NYCViewModel
import javax.inject.Inject

open class BaseFragment : Fragment() {

    @Inject
    lateinit var repository: Repository

    protected val schoolViewModel by lazy {
        ViewModelProvider(this,
            object: ViewModelProvider.Factory{
                override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                    return NYCViewModel(repository) as T
                }
            })[NYCViewModel::class.java]
    }
}