package com.example.nyc_schools_test.view

import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.nyc_schools_test.NycApp
import com.example.nyc_schools_test.R
import com.example.nyc_schools_test.common.SCHOOL_KEY
import com.example.nyc_schools_test.databinding.FragmentScoreBinding
import com.example.nyc_schools_test.model.ResultState
import com.example.nyc_schools_test.model.remote.SatSchool
import com.example.nyc_schools_test.model.remote.SchoolList

class ScoreFragment : BaseFragment() {

    private val binding by lazy {
        FragmentScoreBinding.inflate(layoutInflater)
    }

    private var schoolList: SchoolList? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        NycApp.component.inject(this)

        arguments?.let {
            schoolList = it.getSerializable(SCHOOL_KEY) as SchoolList
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        schoolViewModel.score.observe(viewLifecycleOwner) {
            when (it) {
                is ResultState.Loading -> {

                }
                is ResultState.SuccessSchoolSat -> {
                    displayScores(it.data)
                }
                is ResultState.Error -> {
                    AlertDialog.Builder(requireContext())
                        .setTitle("ERROR")
                        .setMessage(it.errorMessage)
                        .setNegativeButton("DISMISS") { dialog, _ ->
                            dialog.dismiss()
                        }
                }
                else -> {
                    // no-op
                }
            }
        }

        schoolList?.let {
            updateUIElements(it)
            schoolViewModel.getSchoolScores(it.dbn)
        }

        return binding.root
    }

    private fun updateUIElements(schoolList: SchoolList) {
        binding.tvSchoolName.text = schoolList.schoolName
        binding.tvSchoolPhone.text = schoolList.phoneNumber
        binding.tvSchoolWebsite.text = schoolList.website
        binding.tvSchoolLocation.text = schoolList.location
        binding.tvSchoolOverview.text = schoolList.overviewParagraph

        // todo display the name, overview, website, phone number
    }

    private fun displayScores(score: SatSchool? = null) {
        score?.let {
            binding.tvValueNumberOfTestTakers.text = it.satTestTakers
            binding.tvValueCriticalReadingAverage.text = it.readingAvg
            binding.tvValueMathAverage.text = it.mathAvg
            binding.tvValueWritingAverage.text = it.writingAvg
            // todo display scores in the fragment
        }
    }
}