package com.example.nyc_schools_test.view

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.nyc_schools_test.NycApp
import com.example.nyc_schools_test.R
import com.example.nyc_schools_test.adapter.SchoolsAdapter
import com.example.nyc_schools_test.common.SCHOOL_KEY
import com.example.nyc_schools_test.databinding.SchoolFragmentBinding
import com.example.nyc_schools_test.model.ResultState

class FragmentSchool: BaseFragment() {

    private val binding by lazy {
        SchoolFragmentBinding.inflate(layoutInflater)
    }

    private val schoolAdapter by lazy {
        SchoolsAdapter {
            findNavController().navigate(R.id.action_SchoolsFragment_to_ScoreFragment, bundleOf(
                Pair(SCHOOL_KEY, it)
            ))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        NycApp.component.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        binding.schoolsRv.apply {
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            adapter = schoolAdapter
        }

        schoolViewModel.schools.observe(viewLifecycleOwner) {
            when (it) {
                is ResultState.Loading -> {

                }
                is ResultState.SuccessSchoolList -> {
                    schoolAdapter.updateSchools(it.data)
                }
                is ResultState.Error -> {
                    AlertDialog.Builder(requireContext())
                        .setTitle("ERROR")
                        .setMessage(it.errorMessage)
                        .setNegativeButton("DISMISS") { dialog, _ ->
                            dialog.dismiss()
                        }
                }
                else -> {
                    // no-op
                }
            }
        }

        schoolViewModel.getSchoolList()

        return binding.root
    }
}