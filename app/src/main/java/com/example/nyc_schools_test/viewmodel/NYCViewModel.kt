package com.example.nyc_schools_test.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.nyc_schools_test.model.Repository
import com.example.nyc_schools_test.model.ResultState
import kotlinx.coroutines.*

class NYCViewModel(
    private val repository: Repository,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO,
    private val coroutineScope: CoroutineScope = CoroutineScope(ioDispatcher)
    ): ViewModel() {

    private val _schoolList = MutableLiveData<ResultState>(ResultState.Loading(true))
    val schools: LiveData<ResultState>
        get() = _schoolList

    private val _scoresSchool = MutableLiveData<ResultState>(ResultState.Loading(true))
    val score: LiveData<ResultState>
        get() = _scoresSchool

    fun getSchoolList() {
        coroutineScope.launch {
            repository.caseSchoolList()
                .collect {
                    _schoolList.postValue(it)
                }
        }
    }

    fun getSchoolScores(dbn: String) {
        coroutineScope.launch {
            repository.caseSchoolSatByDBN(dbn)
                .collect {
                    _scoresSchool.postValue(it)
                }
        }
    }
}
