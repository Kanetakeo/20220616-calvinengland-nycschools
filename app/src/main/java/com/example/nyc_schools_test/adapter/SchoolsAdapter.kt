package com.example.nyc_schools_test.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.nyc_schools_test.databinding.SchoolItemBinding
import com.example.nyc_schools_test.model.remote.SchoolList

class SchoolsAdapter(
    private val schoolData: MutableList<SchoolList> = mutableListOf(),
    private val onSchoolClick: (SchoolList) -> Unit
) : RecyclerView.Adapter<SchoolViewHolder>() {

    fun updateSchools(newDataSet: List<SchoolList>) {
        schoolData.addAll(newDataSet)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolViewHolder =
        SchoolViewHolder(
            SchoolItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: SchoolViewHolder, position: Int) =
        holder.bind(schoolData[position], onSchoolClick)

    override fun getItemCount(): Int = schoolData.size
}

class SchoolViewHolder(
    private var binding: SchoolItemBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(school: SchoolList, onSchoolClick: (SchoolList) -> Unit) {
        binding.tvSchoolName.text = school.schoolName
        binding.tvSchoolLocation.text = school.location

        itemView.setOnClickListener {
        // this is handling the click on each card view
            onSchoolClick(school)
        }
    }
}