package com.example.nyc_schools_test

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.nyc_schools_test.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private val binding by lazy {
        ActivityMainBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        NycApp.component.inject(this)
        setContentView(binding.root)
    }
}