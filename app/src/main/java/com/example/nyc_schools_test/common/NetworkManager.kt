package com.example.nyc_schools_test.common

import android.net.ConnectivityManager
import android.net.LinkProperties
import android.net.Network
import android.net.NetworkCapabilities
import android.os.Build

class NetworkManager(
    private val connectivityManager: ConnectivityManager
) {

    var isConnected: Boolean = true

    init {
        checkNetworkState()
    }

    private fun checkNetworkState() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            connectivityManager.registerDefaultNetworkCallback(
                object : ConnectivityManager.NetworkCallback() {

                    override fun onAvailable(network: Network) {
                        super.onAvailable(network)
                        isConnected = true
                    }

                    override fun onLost(network: Network) {
                        super.onLost(network)
                        isConnected = false
                    }
                }
            )
        } else {
            val networkInfo = connectivityManager.activeNetworkInfo
            isConnected = networkInfo != null && networkInfo.isConnected
        }
    }
}
