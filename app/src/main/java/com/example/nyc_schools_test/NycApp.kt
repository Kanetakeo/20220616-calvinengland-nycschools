package com.example.nyc_schools_test

import android.app.Application
import com.example.nyc_schools_test.di.ApplicationModule
import com.example.nyc_schools_test.di.DaggerNycComponent
import com.example.nyc_schools_test.di.NycComponent

class NycApp : Application() {

    override fun onCreate() {
        super.onCreate()

        component = DaggerNycComponent.builder()
            .applicationModule(
                ApplicationModule(this)
            )
            .build()
    }

    companion object {
        lateinit var component: NycComponent
    }
}