package com.example.nyc_schools_test.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.nyc_schools_test.model.Repository
import com.example.nyc_schools_test.model.ResultState
import io.mockk.every
import io.mockk.mockk
import io.mockk.unmockkAll
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.TestScope
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
internal class NYCViewModelTest {

    @get: Rule val rule = InstantTaskExecutorRule()

    private val testDispatcher = UnconfinedTestDispatcher()
    private val testScope = TestScope(testDispatcher)

    private val mockRepository = mockk<Repository>(relaxed = true)

    private lateinit var testViewModel: NYCViewModel

    @Before
    fun startup() {
        Dispatchers.setMain(testDispatcher)
        testViewModel = NYCViewModel(mockRepository, testDispatcher, testScope)
    }

    @After
    fun shutdown() {
        unmockkAll()
        Dispatchers.resetMain()
    }

    @Test
    fun `get schools from the server displays loading state`() {
        // assign
        every { mockRepository.caseSchoolList() } returns flowOf(
            ResultState.Loading(true)
        )
        val stateList = mutableListOf<ResultState>()
        testViewModel.schools.observeForever {
            stateList.add(it)
        }

        testViewModel.getSchoolList()

        val isLoading = stateList[0] as ResultState.Loading

        assertEquals(2, stateList.size)
        assertTrue(isLoading.isLoading)
    }

    @Test
    fun `get schools from the server displays success state`() {
        // assign
        every { mockRepository.caseSchoolList() } returns flowOf(
            ResultState.SuccessSchoolList(
                listOf(
                    mockk(),
                    mockk(),
                    mockk()
                )
            )
        )
        val stateList = mutableListOf<ResultState>()
        testViewModel.schools.observeForever {
            stateList.add(it)
        }

        // action
        testViewModel.getSchoolList()

        val isLoading = stateList[0] as ResultState.Loading

        // assertion
        assertEquals(2, stateList.size)
        assertTrue(isLoading.isLoading)
        assertEquals(3, (stateList[1] as ResultState.SuccessSchoolList).data.size)
    }

    @Test
    fun `get schools from the server displays error state`() {
        // assign
        every { mockRepository.caseSchoolList() } returns flowOf(
            ResultState.Error("ERROR")
        )
        val stateList = mutableListOf<ResultState>()
        testViewModel.schools.observeForever {
            stateList.add(it)
        }

        // action
        testViewModel.getSchoolList()

        val isLoading = stateList[0] as ResultState.Loading

        // assertion
        assertEquals(2, stateList.size)
        assertTrue(isLoading.isLoading)
        assertEquals("ERROR", (stateList[1] as ResultState.Error).errorMessage)
    }

    @Test
    fun `get sat scores from the server displays loading state`() {
        // assign
        every { mockRepository.caseSchoolSatByDBN("dbn") } returns flowOf(
            ResultState.Loading(true)
        )
        val stateList = mutableListOf<ResultState>()
        testViewModel.score.observeForever {
            stateList.add(it)
        }

        testViewModel.getSchoolScores("dbn")

        val isLoading = stateList[0] as ResultState.Loading

        assertEquals(2, stateList.size)
        assertTrue(isLoading.isLoading)
    }

    @Test
    fun `get sat scores from the server displays success state`() {
        // assign
        every { mockRepository.caseSchoolSatByDBN("dbn") } returns flowOf(
            ResultState.SuccessSchoolSat( mockk {
                every { dbn } returns "dbn"
            }
            )
        )
        val stateList = mutableListOf<ResultState>()
        testViewModel.score.observeForever {
            stateList.add(it)
        }

        // action
        testViewModel.getSchoolScores("dbn")

        val isLoading = stateList[0] as ResultState.Loading

        // assertion
        assertEquals(2, stateList.size)
        assertTrue(isLoading.isLoading)
        assertEquals("dbn", (stateList[1] as ResultState.SuccessSchoolSat).data?.dbn)
    }

    @Test
    fun `get sat scores from the server displays error state`() {
        // assign
        every { mockRepository.caseSchoolSatByDBN("dbn") } returns flowOf(
            ResultState.Error("ERROR")
        )
        val stateList = mutableListOf<ResultState>()
        testViewModel.score.observeForever {
            stateList.add(it)
        }

        // action
        testViewModel.getSchoolScores("dbn")

        val isLoading = stateList[0] as ResultState.Loading

        // assertion
        assertEquals(2, stateList.size)
        assertTrue(isLoading.isLoading)
        assertEquals("ERROR", (stateList[1] as ResultState.Error).errorMessage)
    }
}